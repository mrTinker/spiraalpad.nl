FROM nginx:alpine
LABEL author="Freek van Keulen"
COPY . dist /usr/share/nginx/html
EXPOSE 80 443
ENTRYPOINT ["nginx","-g", "deamon off;"]
