import {Routes} from '@angular/router';
import {LandingComponent} from './landing/landing.component';
import {UitgangspuntenComponent} from './uitgangspunten/uitgangspunten.component';
import {ActiesComponent} from './acties/acties.component';
import {HoofdstadComponent} from './poi/hoofdstad/hoofdstad.component';
import {PoiOverzichtComponent} from './poi/overzicht/poi-overzicht.component';

export const appRoutes: Routes = [
  {path: 'acties', component: ActiesComponent},
  {path: 'home', component: LandingComponent},
  {path: 'uitgangspunten', component: UitgangspuntenComponent},
  {path: 'poi', component: PoiOverzichtComponent},
  {path: 'poi/hoofdstad', component: HoofdstadComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'}
];
