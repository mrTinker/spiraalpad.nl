import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPoiComponent } from './landing-poi.component';

describe('LandingPoiComponent', () => {
  let component: LandingPoiComponent;
  let fixture: ComponentFixture<LandingPoiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingPoiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingPoiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
