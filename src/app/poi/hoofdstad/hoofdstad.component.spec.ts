import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HoofdstadComponent } from './hoofdstad.component';

describe('HoofdstadComponent', () => {
  let component: HoofdstadComponent;
  let fixture: ComponentFixture<HoofdstadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoofdstadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoofdstadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
