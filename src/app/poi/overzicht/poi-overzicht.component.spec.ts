import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoiOverzichtComponent } from './poi-overzicht.component';

describe('PoiOverzichtComponent', () => {
  let component: PoiOverzichtComponent;
  let fixture: ComponentFixture<PoiOverzichtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoiOverzichtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoiOverzichtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
