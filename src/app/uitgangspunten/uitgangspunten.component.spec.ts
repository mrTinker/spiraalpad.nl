import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UitgangspuntenComponent } from './uitgangspunten.component';

describe('UitgangspuntenComponent', () => {
  let component: UitgangspuntenComponent;
  let fixture: ComponentFixture<UitgangspuntenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UitgangspuntenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UitgangspuntenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
