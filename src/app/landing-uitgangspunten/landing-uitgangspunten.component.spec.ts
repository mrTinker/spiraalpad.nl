import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingUitgangspuntenComponent } from './landing-uitgangspunten.component';

describe('LandingUitgangspuntenComponent', () => {
  let component: LandingUitgangspuntenComponent;
  let fixture: ComponentFixture<LandingUitgangspuntenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingUitgangspuntenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingUitgangspuntenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
