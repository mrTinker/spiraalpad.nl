import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-uitgangspunten',
  templateUrl: './landing-uitgangspunten.component.html',
  styleUrls: ['./landing-uitgangspunten.component.css']
})
export class LandingUitgangspuntenComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
