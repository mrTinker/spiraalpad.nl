import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingActiesComponent } from './landing-acties.component';

describe('LandingActiesComponent', () => {
  let component: LandingActiesComponent;
  let fixture: ComponentFixture<LandingActiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingActiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingActiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
