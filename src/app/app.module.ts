import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { appRoutes} from './routes';
import { AppComponent } from './app.component';
import { LandingPoiComponent } from './landing-poi/landing-poi.component';
import { RouterModule } from '@angular/router';
import { LandingUitgangspuntenComponent } from './landing-uitgangspunten/landing-uitgangspunten.component';
import { LandingComponent } from './landing/landing.component';
import { LandingAchtergrondComponent } from './landing-achtergrond/landing-achtergrond.component';
import { LandingActiesComponent } from './landing-acties/landing-acties.component';
import { UitgangspuntenComponent } from './uitgangspunten/uitgangspunten.component';
import { ActiesComponent } from './acties/acties.component';
import { HoofdstadComponent } from './poi/hoofdstad/hoofdstad.component';
import { PoiOverzichtComponent } from './poi/overzicht/poi-overzicht.component';

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [
    AppComponent,
    LandingPoiComponent,
    LandingUitgangspuntenComponent,
    LandingComponent,
    LandingAchtergrondComponent,
    LandingActiesComponent,
    UitgangspuntenComponent,
    ActiesComponent,
    HoofdstadComponent,
    PoiOverzichtComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
