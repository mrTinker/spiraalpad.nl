import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingAchtergrondComponent } from './landing-achtergrond.component';

describe('LandingAchtergrondComponent', () => {
  let component: LandingAchtergrondComponent;
  let fixture: ComponentFixture<LandingAchtergrondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingAchtergrondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingAchtergrondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
